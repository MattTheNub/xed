# XED

(pronounced zeed)

### Usage: xed path/to/file

### Now how do I actually use it to edit programs?

To read the file you selected you can use `c` to see the full file. To see a single line, type `r`, hit enter, and then the line number, hitting enter again. To edit a line, run `l`, hit enter, and then the line number. Hitting enter again switches you back to command mode. To save, hit enter, than `s` and enter again. To quit, run the same command as you do to save, but `q` instead of `s`.

### Reasons to use this editor:

 * N/A
